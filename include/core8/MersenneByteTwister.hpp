#ifndef CORE8_MERSENNETWISTER_HPP
#define CORE8_MERSENNETWISTER_HPP

#include <random>

#include "global.h"
#include "Chip8.hpp"

namespace Core8 {

class CORE8_EXPORT MersenneByteTwister : public RandomNumberGenerator {
  public:
    MersenneByteTwister() = default;
    ~MersenneByteTwister() override = default;

    Chip8::BYTE get() override {
      return static_cast<Chip8::BYTE>(m_distribution(m_generator));
    }

  private:
    std::mt19937 m_generator{std::random_device{}()};
    std::uniform_int_distribution<unsigned long int> m_distribution{0x01, 0xFF};
};

} // namespace Core8

#endif