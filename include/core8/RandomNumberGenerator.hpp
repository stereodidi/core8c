#ifndef CORE8_RANDOMNUMBERGENERATOR_HPP
#define CORE8_RANDOMNUMBERGENERATOR_HPP

#include "Chip8.hpp"
#include "global.h"

namespace Core8 {

class CORE8_EXPORT RandomNumberGenerator {
  public:
    virtual ~RandomNumberGenerator() = default;

    virtual Chip8::BYTE get() = 0;
};

} // namespace Core8

#endif