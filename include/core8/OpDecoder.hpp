#ifndef CORE8_OPDECODER_H
#define CORE8_OPDECODER_H

#include "Chip8.hpp"
#include "global.h"

namespace Core8 {
namespace OpDecoder {

CORE8_EXPORT Chip8::OpCode decode(const Chip8::WORD word);

} // namespace OpDecoder
} // namespace Core8

#endif