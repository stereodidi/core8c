#include <catch.hpp>

#include "misc/Misc.hpp"
#include "Chip8.hpp"
#include "CPU.hpp"

namespace {

struct CpuFixture {
  Aux::TestKit testKit;
  Core8::CPU& cpu = testKit.cpu;
};

SCENARIO_METHOD(
    CpuFixture,
    "CPUs can execute OR operations on registers with opcode 8XY1",
    "[bitwise]"
) {
  GIVEN("A CPU with some initialized registers") {
    cpu.writeRegister(Core8::Chip8::Register::V0, 0b00001000);
    cpu.writeRegister(Core8::Chip8::Register::V6, 0b01010101);
    cpu.writeRegister(Core8::Chip8::Register::VA, 0b00001111);
    cpu.writeRegister(Core8::Chip8::Register::VE, 0b11001100);

    WHEN("the CPU executes an 8XY1 opcode") {
      cpu.execute(0x8061);
      cpu.execute(0x8AE1);

      THEN("the register VX holds the result of VX OR VY") {
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::V0) == 0b01011101);
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::VA) == 0b11001111);
      }
      AND_THEN("the register VY remains unchanged") {
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::V6) == 0b01010101);
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::VE) == 0b11001100);
      }
    }
  }
}

SCENARIO_METHOD(
    CpuFixture,
    "CPUs can execute AND operations on registers with opcode 8XY2",
    "[bitwise]"
) {
  GIVEN("A CPU with some initialized registers") {
    cpu.writeRegister(Core8::Chip8::Register::V1, 0b01011000);
    cpu.writeRegister(Core8::Chip8::Register::V7, 0b01010101);
    cpu.writeRegister(Core8::Chip8::Register::VB, 0b00001111);
    cpu.writeRegister(Core8::Chip8::Register::VF, 0b11001100);

    WHEN("the CPU executes an 8XY2 opcode") {
      cpu.execute(0x8172);
      cpu.execute(0x8BF2);

      THEN("the register VX holds the result of VX AND VY") {
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::V1) == 0b01010000);
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::VB) == 0b00001100);
      }
      AND_THEN("the register VY remains unchanged") {
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::V7) == 0b01010101);
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::VF) == 0b11001100);
      }
    }
  }
}

SCENARIO_METHOD(
    CpuFixture,
    "CPUs can execute XOR operations on registers with opcode 8XY3",
    "[bitwise]"
) {
  GIVEN("A CPU with some initialized registers") {
    cpu.writeRegister(Core8::Chip8::Register::V2, 0b01011000);
    cpu.writeRegister(Core8::Chip8::Register::V8, 0b01010101);
    cpu.writeRegister(Core8::Chip8::Register::VC, 0b00001111);
    cpu.writeRegister(Core8::Chip8::Register::VD, 0b11001100);

    WHEN("the CPU executes an 8XY3 opcode") {
      cpu.execute(0x8283);
      cpu.execute(0x8CD3);

      THEN("the register VX holds the result of VX XOR VY") {
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::V2) == 0b00001101);
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::VC) == 0b11000011);
      }
      AND_THEN("the register VY remains unchanged") {
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::V8) == 0b01010101);
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::VD) == 0b11001100);
      }
    }
  }
}

SCENARIO_METHOD(
    CpuFixture,
    "CPUs can shift registers right by one with 8XY6 opcode",
    "[bitwise]"
) {
  GIVEN("A CPU with some initialized registers") {
    cpu.writeRegister(Core8::Chip8::Register::V4, 0b01011000);
    cpu.writeRegister(Core8::Chip8::Register::VD, 0b01010101);

    WHEN("the CPU executes an 8XY6 opcode") {
      cpu.execute(0x8406);
      const auto vf1 = cpu.readRegister(Core8::Chip8::Register::VF);

      cpu.execute(0x8D06);
      const auto vf2 = cpu.readRegister(Core8::Chip8::Register::VF);

      THEN("the target register VX is shifted right by one") {
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::V4) == 0b00101100);
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::VD) == 0b00101010);
      }
      AND_THEN("VF is set to the LSB of the target register before the shift") {
        REQUIRE(vf1 == 0b0);
        REQUIRE(vf2 == 0b1);
      }
    }
  }
}

SCENARIO_METHOD(
    CpuFixture,
    "CPUs can shift registers left by one with 8XYE opcode",
    "[bitwise]"
) {
  GIVEN("A CPU with some initialized registers") {
    cpu.writeRegister(Core8::Chip8::Register::VC, 0b11111111);
    cpu.writeRegister(Core8::Chip8::Register::VD, 0b01010101);

    WHEN("the CPU executes an 8XYE opcode") {
      cpu.execute(0x8C0E);
      const auto vf1 = cpu.readRegister(Core8::Chip8::Register::VF);

      cpu.execute(0x8D0E);
      const auto vf2 = cpu.readRegister(Core8::Chip8::Register::VF);

      THEN("the target register VX is shifted left by one") {
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::VC) == 0b11111110);
        REQUIRE(cpu.readRegister(Core8::Chip8::Register::VD) == 0b10101010);
      }
      AND_THEN("VF is set to the MSB of the target register before the shift") {
        REQUIRE(vf1 == 0b1);
        REQUIRE(vf2 == 0b0);
      }
    }
  }
}

} // namespace